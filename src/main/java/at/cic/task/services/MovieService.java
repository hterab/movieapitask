package at.cic.task.services;

import at.cic.task.controllers.MovieController;
import at.cic.task.dtos.MovieDto;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Terab on 26.01.2017.
 */
@Service("movieService")
public class MovieService {
    private static final Logger LOG = LogManager.getLogger(MovieService.class);

    private MovieDto[] movies;



    public MovieService() {
        RestTemplate restTemplate = new RestTemplate();
        movies = restTemplate.getForObject("http://data.sfgov.org/resource/wwmu-gmzc.json?$limit=2000", MovieDto[].class);
    }


    public List<MovieDto> loadAllMovies() {

        LOG.info("Movies found: " + movies.length);
        return Arrays.asList(movies);
    }

    public List<MovieDto> searchMovie(String title) {
     List<MovieDto> foundMovies = new ArrayList<MovieDto>();
        if (movies != null && movies.length > 0) {

            for (MovieDto movieDto : movies) {
                if (movieDto.getTitle().equals(title)) {
                    foundMovies.add(movieDto);
                }
            }
            if(foundMovies.isEmpty())
                return null;

            return foundMovies;
        }
        return null;
    }
}
