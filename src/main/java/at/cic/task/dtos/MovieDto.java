package at.cic.task.dtos;

/**
 * Created by Terab on 26.01.2017.
 */
public class MovieDto {

    private String title;

    private String locations;

    public MovieDto() {

    }

    public MovieDto(String title, String locations) {
        this.title = title;
        this.locations = locations;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }
}
