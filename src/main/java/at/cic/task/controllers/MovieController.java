package at.cic.task.controllers;

import at.cic.task.dtos.MovieDto;
import at.cic.task.services.MovieService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Terab on 26.01.2017.
 */

@RestController
@RequestMapping("/movie")
public class MovieController {
    @Autowired
    private MovieService movieService;

    @RequestMapping(value="/all",  method = RequestMethod.GET, headers = "Accept=application/json",
            produces = "application/json")
    public List<MovieDto> loadAll(){
        return movieService.loadAllMovies();
    }



    @RequestMapping(value="/find/{title}",
            method = RequestMethod.GET,
            headers = "Accept=application/json",
            produces = "application/json")
    public ResponseEntity<List<MovieDto>> searchMovieByTitle(@PathVariable("title") String title) {

        List<MovieDto> movies = movieService.searchMovie(title);
        if(movies != null)
            return new ResponseEntity<List<MovieDto>>(movies, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);


    }
}
